# PHP Rendering Engine Service Provider for Silex

## Build Status
### master branch
[ ![Codeship Status for krossoverintelligence/silex-php-engine](https://www.codeship.io/projects/69f6edd0-a887-0131-ce7d-6241eae100f0/status?branch=master)](https://www.codeship.io/projects/19152)

### QA branch
[ ![Codeship Status for krossoverintelligence/silex-php-engine](https://www.codeship.io/projects/69f6edd0-a887-0131-ce7d-6241eae100f0/status?branch=qa)](https://www.codeship.io/projects/19152)

This Silex service provider will register the Symfony2 rendering engine
for Silex.  It **will** play nicely with the [Silex form service provider](http://silex.sensiolabs.org/doc/providers/form.html).

This was inspired heavily by [Bernhard Schussek's helpful example of using
the Symfony2 form component in a standalone context](https://github.com/bschussek/standalone-forms/blob/2.1%2Bphp/src/setup.php).

## How To Use It

First, register the provider.  If using this in conjunction with the
[Silex translation service provider](http://silex.sensiolabs.org/doc/providers/translation.html)
or the
[Silex form service provider](http://silex.sensiolabs.org/doc/providers/form.html),
you must register this **after** both of those have been registered.

There are two required parameters:

    :::php
        $app->register(
            new \Krossover\SilexPhpEngine\PhpEngineServiceProvider(),
            array(
                'view.directory' => realpath(__DIR__ . '/../Views'),
                'view.vendorDirectory' => realpath(__DIR__ . '/../../vendor'),
            )
        );

Additionally, this contains a trait which adds the following shortcut:

    :::php
        public function render($view, array $parameters = array());

        public function setSlot($slotName, $value);

Note that, unlike the Twig trait distributed with Silex, this one returns
a string, not a Response object.
