<?php namespace Krossover\SilexPhpEngine;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2013 Krossover Intelligence <http://krossover.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

use Silex\ServiceProviderInterface;
use Symfony\Component\Templating\Helper\SlotsHelper;
use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\Loader\FilesystemLoader;
use Symfony\Bundle\FrameworkBundle\Templating\Helper\TranslatorHelper;
use Symfony\Component\Form;

/**
 * The Silex service provider for the Symfony2 PHP rendering engine.
 *
 * This depends on two variables having been set in the configuration:
 *
 * view.directory           The directory where your view files live
 * view.vendorDirectory     The composer vendor directory
 *
 * @package Krossover\SilexPhpEngine
 */
class PhpEngineServiceProvider implements ServiceProviderInterface
{

    /**
     * @param \Silex\Application $app An Application instance
     */
    public function register(\Silex\Application $app)
    {

        $app['view.helpers'] = $app->share(function ($app) {

            $helpers = array();

            if (isset($app['translator'])) {
                $helpers[] = new TranslatorHelper($app['translator']);
            }

            $helpers[] = new SlotsHelper();

            return $helpers;
        });

        if (isset($app['form.extensions'])) {

            // OK, we're using the form service. That means we must load
            // the templating extension, or else it won't play nicely with
            // the PHP rendering engine.

            $extensions = $app['form.extensions'];

            $app['form.extensions'] = $app->share(function ($app) use ($extensions) {


                $vendorFrameworkBundleDir =
                    "{$app['view.vendorDirectory']}/symfony/framework-bundle/Symfony/Bundle/FrameworkBundle";

                $extensions[] = new Form\Extension\Templating\TemplatingExtension(
                    $app['view'],
                    null,
                    array(
                        $vendorFrameworkBundleDir . '/Resources/views/Form',
                    )
                );

                return $extensions;
            });
        }

        $app['view'] = $app->share(function ($app) {
            $engine = new PhpEngine(
                new SimpleTemplateNameParser($app['view.directory']),
                new FilesystemLoader(array())
            );

            $engine->addHelpers($app['view.helpers']);

            return $engine;
        });
    }

    /**
     * @param \Silex\Application $app
     */
    public function boot(\Silex\Application $app)
    {
    }
}
