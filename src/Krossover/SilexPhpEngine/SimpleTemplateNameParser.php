<?php namespace Krossover\SilexPhpEngine;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2013 Krossover Intelligence <http://krossover.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

use Symfony\Component\Templating\TemplateReference;
use Symfony\Component\Templating\TemplateNameParserInterface;

/**
 * A simple template parser to use with the PHP rendering engine.
 * Why not just use the standard symfony2 template parser?  Well, if you
 * use this in conjunction with the Silex Form service, then for some reason
 * that template provider doesn't work.  That template provider typically
 * takes a relative file path along with a FileSystem object which has been
 * initialized to the root directory, but that doesn't appear to work with the
 * Form provider.
 *
 * This is pretty much a straight up copy-paste job from Bernhard Schussek's
 * example.
 *
 * @see https://github.com/bschussek/standalone-forms/blob/2.1%2Bphp/src/setup.php
 * @package Krossover\SilexPhpEngine
 */
class SimpleTemplateNameParser implements TemplateNameParserInterface
{
    private $root;

    /**
     * @param string $root The directory in which template files can be found
     */
    public function __construct($root)
    {
        $this->root = $root;
    }

    /**
     * Parses a view name. Names can use colons ':' to denote sub-directories.
     * Names should NOT include '.php' extensions.
     *
     * @param string $name The name or path to a view file.
     * @return TemplateReference
     */
    public function parse($name)
    {
        $name = trim($name, '/:');
        if (strpos($name, ':') !== false) {
            $name = str_replace(':', '/', $name);
        }

        return new TemplateReference("{$this->root}/{$name}.php", 'php');
    }
}
