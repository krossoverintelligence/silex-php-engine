#!/bin/sh
if [ -f ~/.bash_profile ]; then
    source ~/.bash_profile
fi

cd build
php ../vendor/bin/phing -logger phing.listener.DefaultLogger test

code=$?

if [ "$code" == "0" ]; then
    exit 0
fi

echo "*** Not all tests pass. Fix the problems!"
echo "*** (or commit again with --no-verify if you know what you're doing)"
echo

exit $code
